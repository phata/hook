<?php

/**
 * This file defines Phata\Hook\Invoker, the main interface
 * to invoke hooks in the registry.
 *
 * PHP version >= 7.0
 *
 * @category  Invoker
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook;

use Psr\Container\ContainerInterface;
use Phata\Hook\Exceptions\ClassNotFoundInContainer;

/**
 * An invoker to invoke hook defined in a given registry. Provides
 * multiple manners to invoke the implementations with.
 *
 * It will sort implementations by weight. If an implementation is
 * implementing the SortableInterface, the weight will be retrieved
 * from the getWeight method. Otherwise, weight would be 0.
 *
 * Implementation of smaller weight goes first. Larger weight goes later.
 *
 * All implementations are supposed to be retrievable from the
 * ContainerInterface given when constructed. And the implementations
 * will be sorted by the invocation methods on invoke.
 *
 * Example use:
 *
 * ```php
 * <?php
 *
 * // use composer autoload file, or any custom autoloader
 * // to load up classes. (use the path specific to your
 * // project).
 * require_once 'vendor/autoload.php';
 *
 * $container = new DI\Container; // or other PHP-11 compliant container
 * $registry = new Phata\Hook\Registry;
 * $invoker = new Phata\Hook\SortingInvoker($registry, $container);
 *
 * // declare hooks and add class to registry
 * // ...
 *
 * // use $invoker to invoke a hook
 *
 * // Use 1: map results
 * $array = $invoker->map(App\MyInterface::class,
 *    function (App\MyInterface $object) {
 *        return $object->doSomething();
 *    });
 *
 * // Use 2: reduce
 * $result = $invoker->reduce(
 *    App\MyInterface::class,
 *    function ($carry, App\MyInterface $object) {
 *        return $object->process($carry);
 *    },
 *    $someInitialValue,
 * );
 * ```
 *
 * See documentation of [Phata\Hook\Registry](./Phata.Hook.Registry.html)
 * for hook declaration and class registering.
 *
 * @category Invoker
 * @package  Phata\Hook
 * @author   Koala Yeung <koalay@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://gitlab.com/phata/hook
 * @since    1.1
 */
class SortingInvoker extends Invoker implements InvokerInterface
{

    /**
     * Get implementations of hook in sorting order.
     * They are sorted by weight given by the "getWeight"
     * method if it implements the SortableInterface.
     *
     * @param string $hook Full namespaced interface name.
     *
     * @return array
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.1
     */
    protected function getSorted(string $hook): array
    {
        $invoker = &$this;
        $objects = array_map(
            function ($class) use ($invoker) {
                return $invoker->getObjectOf($class);
            },
            $this->registry->get($hook)
        );
        usort(
            $objects,
            function ($a, $b) {
                $wa = ($a instanceof SortableInterface) ?
                    $a->getWeight() : 0;
                $wb = ($b instanceof SortableInterface) ?
                    $b->getWeight() : 0;
                return $wa <=> $wb;
            }
        );
        return $objects;
    }

    /**
     * Mapping the class that implements the hook with
     * the given callback. Returns the congregated results
     * as an array.
     *
     * The map order follows the order that classes are added
     * to the Registry given to this Invoker.
     *
     * @param string   $hook     Full namespaced name of the hook.
     * @param callable $callback A callable to map with.
     *                           ```mixed callback(mixed $item)```
     *
     * @return array The mapped result array.
     * @throws Phata\Hook\Exceptions\ClassNotFoundInContainer.
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.1
     */
    public function map(string $hook, callable $callback): array
    {
        $invoker = &$this;
        return array_map(
            function ($object) use ($invoker, $callback) {
                return $callback($object);
            },
            $this->getSorted($hook)
        );
    }

    /**
     * Reducing with the class that implements the hook with
     * the given callback. Returns the congregated results
     * as an array.
     *
     * The map order follows the order that classes are added
     * to the Registry given to this Invoker.
     *
     * @param string   $hook     Full namespaced name of the hook.
     * @param callable $callback A callback to reduce with.
     *                           ```mixed callback(mixed $carry, mixed $item)```
     * @param mixed    $initial  If the optional initial is available,
     *                           it will be used at the beginning of
     *                           the process, or as a final result in
     *                           case there is no class for the hook
     *                           to invoke with.
     *
     * @return mixed The reduced result.
     * @throws Phata\Hook\Exceptions\ClassNotFoundInContainer.
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.1
     */
    public function reduce(string $hook, callable $callback, $initial=null)
    {
        $invoker = &$this;
        return array_reduce(
            $this->getSorted($hook),
            function ($carry, $object) use ($invoker, $callback) {
                return $callback($carry, $object);
            },
            $initial
        );
    }

}
