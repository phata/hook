<?php

/**
 * This file defines Phata\Hook\Invoker, the main interface
 * to invoke hooks in the registry.
 *
 * PHP Version >= 7.0
 *
 * @category  Invoker
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook;

/**
 * An invoker to invoke hook defined in a given registry. Provides
 * multiple manners to invoke the implementations with.
 *
 * All implementations are supposed to be retrievable from the
 * ContainerInterface given when constructed.
 *
 * See documentation of [Phata\Hook\Registry](./Phata.Hook.Registry.html)
 * and [Phata\Hook\Invoker](./Phata.Hook.Invoker.html)
 * for hook declaration and class registering.
 *
 * @category Invoker
 * @package  Phata\Hook
 * @author   Koala Yeung <koalay@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://gitlab.com/phata/hook
 * @since    1.1
 */
interface InvokerInterface
{

    /**
     * Mapping the class that implements the hook with
     * the given callback. Returns the congregated results
     * as an array.
     *
     * The map order follows the order that classes are added
     * to the Registry given to this Invoker.
     *
     * @param string   $hook     Full namespaced name of the hook.
     * @param callable $callback A callable to map with.
     *                           ```mixed callback(mixed $item)```
     *
     * @return array The mapped result array.
     * @throws Phata\Hook\Exceptions\ClassNotFoundInContainer.
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.1
     */
    public function map(string $hook, callable $callback): array;

    /**
     * Reducing with the class that implements the hook with
     * the given callback. Returns the congregated results
     * as an array.
     *
     * The map order follows the order that classes are added
     * to the Registry given to this Invoker.
     *
     * @param string   $hook     Full namespaced name of the hook.
     * @param callable $callback A callback to reduce with.
     *                           ```mixed callback(mixed $carry, mixed $item)```
     * @param mixed    $initial  If the optional initial is available,
     *                           it will be used at the beginning of
     *                           the process, or as a final result in
     *                           case there is no class for the hook
     *                           to invoke with.
     *
     * @return mixed The reduced result.
     * @throws Phata\Hook\Exceptions\ClassNotFoundInContainer.
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.1
     */
    public function reduce(string $hook, callable $callback, $initial=null);

}
