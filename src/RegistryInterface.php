<?php

/**
 * This file defines Phata\Hook\RegistryInterface, the
 * interface for Phata\Hook\Invoker to work with.
 *
 * PHP version >= 7.0
 *
 * @category  Registry
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook;

/**
 * An interface of generic hook registry for Phata\Hook\Invoker to
 * invoke with.
 *
 * @category Registry
 * @package  Phata\Hook
 * @author   Koala Yeung <koalay@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://gitlab.com/phata/hook
 * @since    1.1
 */
interface RegistryInterface
{

    /**
     * Get an array of class that implements certain hook.
     *
     * The method should always returns an array. Even if the hook
     * specified is not declared or has no implementation.
     *
     * @param string $hook A full namespaced interface name string.
     *
     * @return string[] An array of full namespaced class name string.
     */
    public function get(string $hook): array;
}
