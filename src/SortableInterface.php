<?php

/**
 * This file defines Phata\Hook\SortableInterface, interface for sorting
 * by Phata\Hook\SortingInvoker.
 *
 * PHP Version >= 7.0
 *
 * @category  Interface
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook;

/**
 * SortableInterface for sorting in Phata\Hook\SortingInvoker.
 *
 * See documentation of [Phata\Hook\SortingInvoker](./Phata.Hook.SortingInvoker.html)
 * for usage.
 *
 * @category Interface
 * @package  Phata\Hook
 * @author   Koala Yeung <koalay@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://gitlab.com/phata/hook
 * @since    1.1
 */
interface SortableInterface
{

    /**
     * Get the weight for sorting. The bigger the weight,
     * the later it will be invoked by SortingInvoker.
     *
     * @since  1.1
     * @return integer
     */
    public function getWeight(): int;

}
