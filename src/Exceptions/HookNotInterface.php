<?php

/**
 * This file defines Phata\Hook\Exceptions\HookNotInterface
 * an Exception class to be used by Phata\Hook\Registry.
 *
 * PHP version >= 7.0
 *
 * @category  Exception
 * @package   Phata\Hook\Exceptions
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook\Exceptions;

use \Exception;

/**
 * Exception to throw if a hook added to Phata\Hook\Registry
 * is not a PHP interface.
 *
 * @category Exception
 * @package  Phata\Hook\Exceptions
 * @author   Koala Yeung <koalay@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://gitlab.com/phata/hook
 * @since    1.0
 */
class HookNotInterface extends Exception
{

    /**
     * Class constructor
     *
     * @param string $hook Full namespaced name of the hook
     *                     to declare.
     */
    public function __construct(string $hook)
    {
        parent::__construct("{$hook} is not an interface");
    }

}
