<?php

/**
 * This file defines Phata\Hook\Exceptions\ClassNotFoundInContainer,
 * an Exception class to be used by Phata\Hook\Registry.
 *
 * PHP version >= 7.0
 *
 * @category  Exception
 * @package   Phata\Hook\Exceptions
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook\Exceptions;

use \Exception;

/**
 * Exception to throw if Phata\HookInvoker cannot find a class
 * to invoke with.
 *
 * @category Exception
 * @package  Phata\Hook\Exceptions
 * @author   Koala Yeung <koalay@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://gitlab.com/phata/hook
 * @since    1.0
 */
class ClassNotFoundInContainer extends Exception
{

    /**
     * Class constructor
     *
     * @param string $class Full namespaced name of the class.
     */
    public function __construct(string $class)
    {
        parent::__construct("Class {$class} not found in the container");
    }
}
