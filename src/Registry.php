<?php

/**
 * This file defines Phata\Hook\Registry, the main interface
 * to use this library.
 *
 * PHP Version >= 7.0
 *
 * @category  Registry
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */
namespace Phata\Hook;

use Phata\Hook\Exceptions\HookNotInterface;
use Phata\Hook\Exceptions\ClassNotExists;
use \ReflectionClass;
use \ReflectionException;

/**
 * A generic hook registry class.
 *
 * A "hook" means a specific PHP Interface string, with proper namespace.
 * This is a serializable PHP object for storing the hook-class
 * relationship for cache or storage.
 *
 * A `Phata\Hook\Invoker` will be used to invoke any hook in the registry.
 *
 * Example use:
 *
 * ```php
 * <?php
 *
 * // use composer autoload file, or any custom autoloader
 * // to load up classes. (use the path specific to your
 * // project).
 * require_once 'vendor/autoload.php';
 *
 * $container = new DI\Container; // or other PHP-11 compliant container
 * $registry = new Phata\Hook\Registry;
 * $invoker = new Phata\Hook\Invoker($registry, $container);
 *
 * // declare existing interfaces as hook.
 * $registry->declareHooks([
 *     App\MyInterface::class,
 *     SomeAwesomeLibrary\Interface::class,
 * ]);
 *
 * // add class to the registry, which would
 * // automatically index against the declared
 * // hooks.
 * $registry->add([
 *     App\MyModule1::class,
 *     App\MyModule2::class,
 *     FooBar\External\Module::class,
 * ]);
 *
 * // use $invoker to invoke a hook
 * // ...
 * ```
 *
 * See documentation of [Phata\Hook\Invoker](./Phata.Hook.Invoker.html)
 * for invoke methods.
 *
 * @category Registry
 * @package  Phata\Hook
 * @author   Koala Yeung <koalay@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://gitlab.com/phata/hook
 * @since    1.0
 */
class Registry implements RegistryInterface
{

    /**
     * Array of PHP Interface  name string, with namespace.
     *
     * @var string[]
     */
    private $_hooks = [];

    /**
     * Array of class name string, with namespace.
     *
     * @var string[]
     */
    private $_classes = [];

    /**
     * Array of the classes mapped to the hooks.
     *
     * @var string[]
     */
    private $_index = [];

    /**
     * Declare that an Interface as a hook.
     *
     * The registry will index the classes under the defined hooks.
     *
     * @param string[] $hooks Array of full namespaced interface name.
     *
     * @return Phata\Hook\Registry
     * @throws Phata\Hook\Exceptions\HookNotInterface
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.0
     */
    public function declareHooks(array $hooks): Registry
    {
        // merge all hooks into the current registry
        $this->_hooks = array_merge(
            $this->_hooks,
            array_reduce(
                $hooks,
                function ($carry, $hook) {
                    try {
                        $reflect = new ReflectionClass($hook);
                    } catch (ReflectionException $e) {
                        throw new HookNotInterface($hook);
                    }
                    if (!$reflect->isInterface()) {
                        throw new HookNotInterface($hook);
                    }
                    $carry[] = $hook;
                    return $carry;
                }
            )
        );
        return $this;
    }

    /**
     * Check if a hook is declared in this registry.
     *
     * @param string $hook A full namespaced interface or abstract class
     *                     name to check with.
     *
     * @return boolean If the hook is declared in the registry.
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.0
     */
    public function hasHook(string $hook): bool
    {
        return in_array($hook, $this->_hooks);
    }

    /**
     * Register classes that are supposed to have implemented
     * one or multiple hook.
     *
     * @param string[] $classes Array of full namespaced classes.
     *
     * @return Phata\Hook\Registry
     * @throws Phata\Hook\Exceptions\ClassNotExists
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.0
     */
    public function add(array $classes): Registry
    {
        foreach ($classes as $class) {

            if (!class_exists($class)) {
                throw new ClassNotExists($class);
            }

            // add the class to classes array
            $this->_classes[] = $class;

            // get the implemented hooks
            $registry = &$this;
            $hooks = array_keys(
                array_filter(
                    array_map(
                        function ($implemented) use ($registry) {
                            return $registry->hasHook($implemented);
                        },
                        class_implements($class)
                    ),
                    function ($result) {
                        return $result;
                    }
                )
            );

            // add class to index
            foreach ($hooks as $hook) {
                $this->_index[$hook][] = $class;
            }
        }
        return $this;
    }

    /**
     * Get an array of class that implements certain hook.
     *
     * The method should always returns an array. Even if the hook
     * specified is not declared or has no implementation.
     *
     * @param string $hook Full namespaced named of an interface.
     *
     * @return string[]
     *
     * @author Koala Yeung <koalay@gmail.com>
     * @since  1.0
     */
    public function get(string $hook): array
    {
        return isset($this->_index[$hook]) ?
            $this->_index[$hook] : [];
    }

}
