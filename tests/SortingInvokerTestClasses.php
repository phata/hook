<?php

/**
 * Class to test Phata\Hook\SortingInvoker with.
 *
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook\Test\SortingInvoker;

use Psr\Container\ContainerInterface;

class Container implements ContainerInterface {

    private $_contained = [];

    /**
     * Add value mapped to $id
     *
     * @param mixed $id
     * @param mixed $value
     *
     * @return void
     */
    public function add($id, $value)
    {
        $this->_contained[$id] = &$value;
    }

    /**
     * @inheritDoc
     */
    public function get($id)
    {
        return isset($this->_contained[$id]) ?
            $this->_contained[$id]:
            null;
    }

    /**
     * @inheritDoc
     */
    public function has($id)
    {
        return isset($this->_contained[$id]);
    }
}

namespace Phata\Hook\Test\SortingInvoker\DummyPackage1;

trait NamerTrait {
    public function name(string $locale): string {
        // simply return last part of the class name
        return preg_replace('/.*\\\\(.+?)$/', '$1', static::class);
    }
}

interface Namer {
    public function name(string $locale): string;
}

namespace Phata\Hook\Test\SortingInvoker\DummyPackage2;

interface Speaker {
    public function speak(string $locale): string;
}

namespace Phata\Hook\Test\SortingInvoker\DummyPackage3;

interface Flyer {
    public function fly(string $locale): string;
}

namespace Phata\Hook\Test\SortingInvoker\App;

use \Phata\Hook\Test\SortingInvoker\DummyPackage1\{Namer, NamerTrait};
use \Phata\Hook\Test\SortingInvoker\DummyPackage2\Speaker;
use \Phata\Hook\Test\SortingInvoker\DummyPackage3\Flyer;
use \Phata\Hook\SortableInterface;

class Dog implements Namer, Speaker {

    use NamerTrait;

    public function speak(string $locale): string
    {
        return "Woof";
    }
}

class Cat implements SortableInterface, Namer, Speaker {

    use NamerTrait;

    public function speak(string $locale): string
    {
        return "Meao";
    }

    public function getWeight(): int
    {
        return -100;
    }
}

class Bird implements SortableInterface, Namer, Speaker, Flyer {

    use NamerTrait;

    public function speak(string $locale): string
    {
        return "Tweet";
    }

    public function fly(string $locale): string
    {
        return "Flying";
    }

    public function getWeight(): int
    {
        return 100;
    }
}

class Fox implements Namer {
    use NamerTrait;
}


