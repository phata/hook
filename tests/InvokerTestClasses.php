<?php

namespace Phata\Hook\Test\InvokerTest;

use Psr\Container\ContainerInterface;

trait NamerTrait {
    public function name(): string {
        $fullname = explode('\\', static::class);
        return array_pop($fullname);
    }
}
interface DummyInterface {
    public function name(): string;
}

class DummyClassA implements DummyInterface {
    use NamerTrait;
}

class DummyClassB implements DummyInterface {
    use NamerTrait;
}

class DummyClassC implements DummyInterface {
    use NamerTrait;
}

class Container implements ContainerInterface {

    private $_contained = [];

    /**
     * Add value mapped to $id
     *
     * @param mixed $id
     * @param mixed $value
     *
     * @return void
     */
    public function add($id, $value)
    {
        $this->_contained[$id] = &$value;
    }

    /**
     * @inheritDoc
     */
    public function get($id)
    {
        return isset($this->_contained[$id]) ?
            $this->_contained[$id]:
            null;
    }

    /**
     * @inheritDoc
     */
    public function has($id)
    {
        return isset($this->_contained[$id]);
    }
}
