<?php

/**
 * Unit test for Phata\Hook\SortableRegistry
 *
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook\Test\SortingInvoker;

use Phata\Hook\Registry;
use Phata\Hook\SortingInvoker;
use Phata\Hook\SortableInterface;
use Phata\Hook\Test\SortingInvoker\App;

 /**
  * Unit test for Phata\Hook\SortableRegistry
  */
class SortingInvokerTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
        require_once __DIR__ . '/SortingInvokerTestClasses.php';
    }

    protected function _after()
    {
    }

    /**
     * Test Phata\Hook\SortingInvoker::map
     *
     * @return void
     */
    public function testMap(): void
    {
        // registry
        $r = new Registry;
        $r->declareHooks([
            DummyPackage1\Namer::class,
            DummyPackage2\Speaker::class,
            DummyPackage3\Flyer::class,
        ]);
        $r->add([
            App\Dog::class,
            App\Cat::class,
            App\Bird::class,
            App\Fox::class,
        ]);

        // container
        $c = new Container;
        $c->add(App\Dog::class, new App\Dog);
        $c->add(App\Cat::class, new App\Cat);
        $c->add(App\Bird::class, new App\Bird);
        $c->add(App\Fox::class, new App\Fox);

        // invoker to test with
        $i = new SortingInvoker($r, $c);
        $result = implode(', ', $i->map(
            DummyPackage1\Namer::class,
            function (DummyPackage1\Namer $namer) {
                return $namer->name('en');
            }
        ));
        $this->assertEquals($result, 'Cat, Dog, Fox, Bird');
    }

    /**
     * Test Phata\Hook\SortingInvoker::reduce
     *
     * @return void
     */
    public function testReduce(): void
    {
        // registry
        $r = new Registry;
        $r->declareHooks([
            DummyPackage1\Namer::class,
            DummyPackage2\Speaker::class,
            DummyPackage3\Flyer::class,
        ]);
        $r->add([
            App\Dog::class,
            App\Cat::class,
            App\Bird::class,
            App\Fox::class,
        ]);

        // container
        $c = new Container;
        $c->add(App\Dog::class, new App\Dog);
        $c->add(App\Cat::class, new App\Cat);
        $c->add(App\Bird::class, new App\Bird);
        $c->add(App\Fox::class, new App\Fox);

        // invoker to test with
        $i = new SortingInvoker($r, $c);
        $result = implode(', ', $i->reduce(
            DummyPackage1\Namer::class,
            function ($carry, DummyPackage1\Namer $namer) {
                $carry[] = $namer->name('en');
                return $carry;
            },
            []
        ));
        $this->assertEquals($result, 'Cat, Dog, Fox, Bird');
    }

}
