<?php

/**
 * Unit test for Phata\Hook\Registry
 *
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook\Test\Registry;

use Phata\Hook\Registry;

 /**
  * Unit test for Phata\Hook\Registry
  */
class RegistryTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
        require_once __DIR__ . '/RegistryTestClasses.php';
    }

    protected function _after()
    {
    }

    /**
     * Test Phata\Hook\Registry::declareHook
     *
     * @return void
     */
    public function testDeclareHooks(): void
    {
        $r = new Registry;
        $r->declareHooks([
            DummyPackage1\Namer::class,
            DummyPackage2\Speaker::class,
            DummyPackage3\Flyer::class,
        ]);

        $this->assertTrue($r->hasHook(DummyPackage1\Namer::class),
            'DummyPackage1\Namer::class is a registered hook');
        $this->assertTrue($r->hasHook(DummyPackage2\Speaker::class),
            'DummyPackage1\Speaker::class is a registered hook');
        $this->assertTrue($r->hasHook(DummyPackage3\Flyer::class),
            'DummyPackage1\Flyer::class is a registered hook');
        $this->assertFalse($r->hasHook(DummyPackage4\NonExists::class),
            'DummyPackage4\NonExists::class is not a registered hook');
    }

    /**
     * Test Phata\Hook\Registry::declareHook with a non-interface
     *
     * @expectedException Phata\Hook\Exceptions\HookNotInterface
     *
     * @return void
     */
    public function testDeclareHooksWithNonInterface(): void
    {
        $r = new Registry;
        $r->declareHooks([
            App\Dog::class,
        ]);
    }

    /**
     * Test Phata\Hook\Registry::add and Phata\Hook\Registry::get
     *
     * @return void
     */
    public function testAddAndGet()
    {
        $r = new Registry;
        $r->declareHooks([
            DummyPackage1\Namer::class,
            DummyPackage2\Speaker::class,
            DummyPackage3\Flyer::class,
        ]);
        $r->add([
            App\Dog::class,
            App\Cat::class,
            App\Bird::class,
            App\Fox::class,
        ]);

        $namers = $r->get(DummyPackage1\Namer::class);
        $this->assertCount(4, $namers, 'Registry should have 3 namers');

        $speakers = $r->get(DummyPackage2\Speaker::class);
        $this->assertCount(3, $speakers, 'Registry should have 3 speakers');

        $flyers = $r->get(DummyPackage3\Flyer::class);
        $this->assertCount(1, $flyers, 'Registry should have 1 flyer');

        $aliens = $r->get(\Something\Not\Exists::class);
        $this->assertCount(0, $aliens, 'Registry shoud have 0 alien');
    }

    function testGettingNonExistsHook()
    {
        $r = new Registry;
        $results = $r->get(Package\NotExists::class);
        $this->assertTrue(is_array($results), '$results is an array');
        $this->assertCount(0, $results);
    }

    /**
     * Test Phata\Hook\Registry::add with non-exists class.
     *
     * @expectedException Phata\Hook\Exceptions\ClassNotExists
     */
    public function testAddNonExistsClass()
    {
        $r = new Registry;
        $r->add([
            DummyPackage1\NonExists::class,
        ]);
    }
}
