<?php

/**
 * Class to test Phata\Hook\Registry with.
 *
 * @package   Phata\Hook
 * @author    Koala Yeung <koalay@gmail.com>
 * @copyright 2018 - present Koala, Yeung Shu Hung
 * @license   https://opensource.org/licenses/MIT MIT License
 * @link      http://gitlab.com/phata/hook
 */

namespace Phata\Hook\Test\Registry\DummyPackage1;

trait NamerTrait {
    public function name(string $locale): string {
        // simply return last part of the class name
        return preg_replace('/.*\\(.+?)$/', '$1', static::class);
    }
}

interface Namer {
    public function name(string $locale): string;
}

namespace Phata\Hook\Test\Registry\DummyPackage2;

interface Speaker {
    public function speak(string $locale): string;
}

namespace Phata\Hook\Test\Registry\DummyPackage3;

interface Flyer {
    public function fly(string $locale): string;
}

namespace Phata\Hook\Test\Registry\App;

use Phata\Hook\Test\Registry\DummyPackage1\{Namer, NamerTrait};
use Phata\Hook\Test\Registry\DummyPackage2\Speaker;
use Phata\Hook\Test\Registry\DummyPackage3\Flyer;

class Dog implements Namer, Speaker {

    use NamerTrait;

    public function speak(string $locale): string
    {
        return "Woof";
    }
}

class Cat implements Namer, Speaker {

    use NamerTrait;

    public function speak(string $locale): string
    {
        return "Meao";
    }
}

class Bird implements Namer, Speaker, Flyer {

    use NamerTrait;

    public function speak(string $locale): string
    {
        return "Tweet";
    }

    public function fly(string $locale): string
    {
        return "Flying";
    }
}

class Fox implements Namer {
    use NamerTrait;
}
