<?php

use Phata\Hook\Invoker;
use Phata\Hook\Registry;
use Phata\Hook\Test\InvokerTest\{DummyInterface, DummyClassA, DummyClassB, DummyClassC, Container};

class InvokerTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
        require_once __DIR__ . '/InvokerTestClasses.php';
    }

    protected function _after()
    {
    }

    public function testMap()
    {
        $r = new Registry;
        $r->declareHooks([
            DummyInterface::class,
        ]);
        $r->add([
            DummyClassA::class,
        ]);

        $c = new Container;
        $c->add(DummyClassA::class, 'hello');

        $i = new Invoker($r, $c);

        $this->assertEquals('hello', $c->get(DummyClassA::class),
            'Container gets the dummy class');

        $results = $i->map(DummyInterface::class, function($item) {
            return $item;
        });
        $this->assertCount(1, $results, 'There is 1 result');
        $this->assertEquals('hello', $results[0], 'The only item is "hello"');
    }

    /**
     * @expectedException Phata\Hook\Exceptions\ClassNotFoundInContainer
     */
    public function testMapNonExists()
    {
        $r = new Registry;
        $r->declareHooks([
            DummyInterface::class,
        ]);
        $r->add([
            DummyClassA::class,
        ]);
        $c = new Container;
        $i = new Invoker($r, $c);

        $i->map(DummyInterface::class, function ($dummy) {
            return get_class($dummy);
        });
    }

    public function testReduce()
    {
        $r = new Registry;
        $r->declareHooks([
            DummyInterface::class,
        ]);
        $r->add([
            DummyClassA::class,
            DummyClassB::class,
            DummyClassC::class,
        ]);

        $c = new Container;
        $c->add(DummyClassA::class, new DummyClassA());
        $c->add(DummyClassB::class, new DummyClassB());
        $c->add(DummyClassC::class, new DummyClassC());

        $i = new Invoker($r, $c);

        $this->assertEquals('DummyClassA', $c->get(DummyClassA::class)->name(),
            'Container gets the dummy class');

        $result = $i->reduce(DummyInterface::class, function($carry, $object) {
            return trim($carry .= ' ' . $object->name());
        }, '');
        $this->assertEquals('DummyClassA DummyClassB DummyClassC', $result, 'The only item is "hello"');
    }

}
